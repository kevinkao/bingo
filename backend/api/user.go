
package main

import (
	"context"
	"os"
	"time"

	"io/ioutil"
	"golang.org/x/crypto/ssh"

	pb "api/pb"
	jwt "github.com/golang-jwt/jwt/v5"
)

type MyClaims struct {
	UserInfo map[string]string `json:"user_info"`
	jwt.RegisteredClaims
}

type UserService struct {
	pb.UnimplementedUserServiceServer
}

func (a *UserService)Login(ctx context.Context, ac *pb.UserLoginRequest) (*pb.UserLoginResponse, error) {
	bytes, err := ioutil.ReadFile(os.Getenv("JWT_PRIVATE_KEY_FILE"))
	if err != nil {
		panic(err)
	}
	key, err := ssh.ParseRawPrivateKey(bytes)
	if err != nil {
		panic(err)
	}

	claims := MyClaims {
		map[string]string {
			"user_id": "1",
		},
		jwt.RegisteredClaims {
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(24 * time.Hour)),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			Issuer: "KevinKao",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	ss, _ := token.SignedString(key)
	return &pb.UserLoginResponse {
		Token: ss,
	}, nil;
}

func (a *UserService)GetInfo(ctx context.Context, ai *pb.GetInfoRequest) (*pb.UserInfo, error) {
	return &pb.UserInfo {}, nil;
}