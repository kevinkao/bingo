package main

import (
	"net"
	"log"
	"context"
	"fmt"
	"reflect"
	// "strings"

	"google.golang.org/grpc"
	// "google.golang.org/grpc/codes"		// grpc 響應狀態碼
	// "google.golang.org/grpc/credentials"	// grpc認證包
	// "google.golang.org/grpc/metadata"		// grpc metadata包

	pb "api/pb"
)

func main() {
	lis, err := net.Listen("tcp", ":8787");
	if err != nil {
		log.Fatalf("failed to listen %v", err)
	}

	opts := []grpc.ServerOption {
		grpc.UnaryInterceptor(auth),
	}
	
	grpcServer := grpc.NewServer(opts...)

	pb.RegisterUserServiceServer(grpcServer, &UserService{})
	
	grpcServer.Serve(lis)
}

func auth(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	fmt.Println("Received request at:", info.FullMethod)

	// md, ok := metadata.FromIncomingContext(ctx)
	// if !ok {
	// 	return "", grpc.Errorf(codes.Unauthenticated, "無Token認證信息")
	// }
	// authSlice := strings.Fields(md["authorization"][0])
	// if len(authSlice) < 2 {
	// 	return "", grpc.Errorf(codes.Unauthenticated, "Invalid Token format")
	// }
	// token := authSlice[1]
	// fmt.Println(token)

	resp, err := handler(ctx, req)
	fmt.Println(reflect.TypeOf(req))
	fmt.Println(reflect.TypeOf(resp))
	// fmt.Println(resp.(*pb.Account).Name)

	// resp.(*pb.Account).Name = resp.(*pb.Account).Name + " modified"

	fmt.Println("Request processed")
	return resp, err
}