FROM golang:1.21.0 as builder

RUN apt-get update && apt-get install -y protobuf-compiler

RUN go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28
RUN go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2

COPY ./backend/api /app
WORKDIR /app

RUN protoc --go_out=. --go_opt=paths=source_relative \
    --go-grpc_out=. --go-grpc_opt=paths=source_relative \
    pb/*.proto

RUN go mod init api
RUN go mod tidy
RUN go mod download && go mod verify


FROM golang:1.21.0 as compiler
COPY --from=builder /app /app
WORKDIR /app
RUN CGO_ENABLED=0 go build -v -o /app/bin/bingo


FROM alpine:latest
WORKDIR /
COPY --from=compiler /app/bin/bingo /bingo
CMD ["/bingo"]