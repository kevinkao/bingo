const KevinKaoToken = artifacts.require("KevinKaoToken");

/*
 * uncomment accounts to access the test accounts made available by the
 * Ethereum client
 * See docs: https://www.trufflesuite.com/docs/truffle/testing/writing-tests-in-javascript
 */
contract("KevinKaoToken", function (accounts) {
    const toBN = web3.utils.toBN;
    const toDecimal = function(num) {
        return toBN(num).mul(toBN(10).pow(toBN(18)));
    };

    it("should put 10 KKT in the first account", async function () {
        const contract = await KevinKaoToken.new(10, { from: accounts[0] });
        let actual = await contract.totalSupply();
        let expected = toDecimal(10);
        assert.isTrue(actual.eq(expected), "totoal supply doest not match");
        actual = await contract.balanceOf(accounts[0]);
        assert.isTrue(actual.eq(expected), "initial msg.sender not own the all token");
    });

    it ("should transfer 1 from accounts[0] to accounts[1]", async function() {
        const contract = await KevinKaoToken.new(10, { from: accounts[0] });
        let balance = await contract.balanceOf(accounts[0]);
        assert.isTrue(balance.eq(toDecimal(10)));

        await contract.transfer(accounts[1], toDecimal(1), { from: accounts[0] });
        balance = await contract.balanceOf(accounts[1]);
        assert.isTrue(balance.eq(toDecimal(1)));

        balance = await contract.balanceOf(accounts[0]);
        assert.isTrue(balance.eq(toDecimal(9)));
    });
});
